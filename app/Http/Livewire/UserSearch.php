<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Livewire\WithPagination;
use App\Http\Livewire\UserDetails;

class UserSearch extends Component
{   
    use WithPagination;

    public $searchTerm = '';

    public function mount()
    {
        $this->resetPage();
    }

    public function render()
    {   
        $query = User::query();

        if (strlen($this->searchTerm) >= 3) {
            $query->where('name', 'like', $this->searchTerm . '%')
                ->orWhere('email', 'like', $this->searchTerm . '%');
        }

        $users = $query->paginate(10);

        return view('livewire.user-search', [
            'users' => $users,
        ]);
    }

    public function updatedSearchTerm()
    {
        $this->resetPage();
    }

    public function viewUser($userId)
    {   
        return redirect()->route('user.details', $userId);
    }
}
