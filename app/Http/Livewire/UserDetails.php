<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class UserDetails extends Component
{
    use WithFileUploads;

    public $user;
    public $password;
    // public $deletePassword;
    // public $password_confirmation;
    public $email;
    public $confirmingUserDeletion = false;

    protected $rules = [
        'password' => 'required|min:8|confirmed',
        'email' => 'required|email|unique:users,email',
    ];

    public function mount($userId)
    {   
        $this->user = User::findOrFail($userId);
        $this->resetForm();
    }

    public function render()
    {
        return view('livewire.user-details', ['user' => $this->user]);
    }

    public function updateUserPassword()
    {
        $validator = Validator::make(
            ['password' => $this->password],
            [
                'password' => [
                    'required',
                    'min:8',
                    'confirmed',
                ],
            ]
        );

        if ($validator->fails()) {
            $this->addErrorMessages($validator->errors());
            return;
        }

        $this->user->update([
            'password' => Hash::make($this->password),
        ]);

        session()->flash('password_status', 'Password updated successfully.');
        $this->resetForm();

        $this->destroySessionAfterDelay(3000, 'password_status'); // Delay in milliseconds before destroying the session
    }


    public function updateUserEmail()
    {
        $validator = Validator::make(
            ['email' => $this->email],
            [
                'email' => [
                    'required',
                    'email',
                    'unique:users,email',
                ],
            ]
        );

        if ($validator->fails()) {
            $this->addErrorMessages($validator->errors());
            return;
        }

        $this->user->update([
            'email' => $this->email,
        ]);

        session()->flash('email_status', 'Email updated successfully.');
        $this->resetForm();

        $this->destroySessionAfterDelay(3000, 'email_status'); // Delay in milliseconds before destroying the session
    }

    public function confirmUserDeletion()
    {
        $this->resetForm();
        $this->confirmingUserDeletion = true;
    }

    public function cancelUserDeletion()
    {
        $this->confirmingUserDeletion = false;
    }

    public function deleteUser()
    {   
        // if (empty($this->deletePassword)) {
        //     $this->addError('deletePassword', 'Please enter user password.');
        //     return;
        // }

        // if (!Hash::check($this->deletePassword, $this->user->password)) {
        //     $this->addError('deletePassword', 'Incorrect password.');
        //     return;
        // }

        $this->user->delete();

        return redirect()->route('dashboard')->with('success', 'Account deleted successfully.');
    }

    private function resetForm()
    {
        $this->password = '';
        // $this->deletePassword = '';
        // $this->password_confirmation = '';
        $this->email = '';
        $this->resetValidation();
    }

    public function destroySessionAfterDelay($delay, $session_name)
    {   
        if (session()->has($session_name)) {
            sleep($delay / 1000); // Converting delay to seconds
            session()->forget($session_name);
        }
    }

    private function addErrorMessages($errors)
    {
        foreach ($errors->all() as $error) {
            $this->addError('password', $error);
        }
    }
}