<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Http\Livewire\UserDetails;
use App\Http\Livewire\UserSearch;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// setting dashboard route to be default
Route::get('/', function () {
    return redirect()->route('dashboard');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {

    Route::get('/dashboard', function () {
        return view('dashboard', [
            'userSearchComponent' => UserSearch::class,
        ]);
    })->name('dashboard');

    Route::get('/users/{userId}', UserDetails::class)->name('user.details');
});
