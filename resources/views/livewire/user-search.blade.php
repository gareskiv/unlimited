<div>
    
    <label for="search">Search by:</label>
    <br />
    <input type="text" id="search" wire:model.debounce.500ms="searchTerm" placeholder="username or email..." class="mb-4 p-2 border border-gray-300 rounded">

    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-4">
        @foreach ($users as $user)
        <div class="bg-white rounded shadow-md p-2">
            <div class="flex justify-around" style="height:147px;">
                @if ($user->profile_photo_path)
                    <img src="{{ $user->profile_photo_path }}" alt="Profile Picture" class="w-58 h-58 object-cover rounded">
                @endif
                <div class="flex-grow p-4">
                    <p class="text-lg px-2"> <b>Name:</b> {{ $user->name }}</p>
                    <p class="text-lg px-2"> <b>Email:</b> {{ $user->email }}</p>
                </div>
            </div>
            <br />
            <hr />
            <div class="flex justify-end px-4 py-4 my-2" style="height:72px;">
                <button wire:click="viewUser({{ $user->id }})" 
                    class="px-4 py-2 text-sm font-medium text-white rounded-md" 
                    style="border:1px solid rgb(104, 117, 245); background:rgb(104, 117, 245);">
                    View Details
                </button>
            </div>
        </div>
        @endforeach
    </div>
    <br />
    <div class="mt-4">
        {{ $users->links() }}
    </div>
</div>
