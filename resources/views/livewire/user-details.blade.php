<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('User Details') }}
    </h2>
</x-slot>

<div>
    <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
        
        <div class="mt-10 sm:mt-0 bg-white rounded shadow-md p-2">
            <div class="mt-2">
                <div class="flex justify-around">
                    @if ($user->profile_photo_path)
                        <img src="{{ $user->profile_photo_path }}" alt="Profile Picture" class="object-cover rounded" style="height:250px; width:250px;">
                    @endif
                    <div class="flex-grow p-4">
                        <p class="text-lg px-2"> <b>Name:</b> {{ $user->name }}</p>
                        <p class="text-lg px-2"> <b>Email:</b> {{ $user->email }}</p>
                        <p class="text-lg px-2"> <b>Created At:</b> {{ date('m/d/Y', strtotime($user->created_at)) }}</p>
                    </div>
                </div>
            </div>
        </div>

        <x-section-border />

        <div class="mt-10 sm:mt-0 bg-white rounded shadow-md p-2">
            <div class="mt-2">
                <h2 class="text-lg font-semibold">Change Password</h2>
                <div class="mt-4">
                    <form wire:submit.prevent="updateUserPassword" wire:ignore>
                        <div class="mb-4">
                            <label for="password" class="block text-sm font-medium text-gray-700">New Password</label>
                            <input id="password" type="password" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 @error('password') border-red-500 @enderror" wire:model.defer="password" autocomplete="new-password" />
                            @error('password') <span class="text-red-500 text-sm">{{ $message }}</span> @enderror
                        </div>

                        <div class="mb-4">
                            <label for="password_confirmation" class="block text-sm font-medium text-gray-700">Confirm Password</label>
                            <input id="password_confirmation" type="password" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50" wire:model.defer="password_confirmation" autocomplete="new-password" />
                            @error('password_confirmation') <span class="text-red-500 text-sm">{{ $message }}</span> @enderror
                        </div>

                        <div class="flex items-center">
                            <button type="submit" class="px-4 py-2 text-sm font-medium text-white bg-red-600 rounded-md hover:bg-red-500" style="border:1px solid rgb(104, 117, 245); background:rgb(104, 117, 245);">Save</button>
                            @if (session()->has('password_status'))
                                <span class="ml-2 text-sm text-success">{{ session('password_status') }}</span>
                            @endif
                        </div>
                    </form>
                </div>
            </div>                
        </div>

        <x-section-border />

        <div class="mt-10 sm:mt-0 bg-white rounded shadow-md p-2">
            <div class="mt-2">
                <h2 class="text-lg font-semibold">Change Email</h2>
                <div class="mt-4">
                    <form wire:submit.prevent="updateUserEmail" wire:ignore>
                        <div class="mb-4">
                            <label for="email" class="block text-sm font-medium text-gray-700">Email</label>
                            <input id="email" type="email" class="mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 @error('email') border-red-500 @enderror" wire:model.defer="email" />
                            @error('email') <span class="text-red-500 text-sm">{{ $message }}</span> @enderror
                        </div>

                        <div class="flex items-center">
                            <button type="submit" class="px-4 py-2 text-sm font-medium text-white bg-red-600 rounded-md hover:bg-red-500" style="border:1px solid rgb(104, 117, 245); background:rgb(104, 117, 245);">Save</button>
                            @if (session()->has('email_status'))
                                <span class="ml-2 text-sm text-success">{{ session('email_status') }}</span>
                            @endif
                        </div>
                    </form>
                </div>
            </div>                
        </div>

        <x-section-border />

        <div class="mt-10 sm:mt-0 bg-white rounded shadow-md p-2">
            <div class="mt-2">
                <h2 class="text-lg font-semibold">Delete Account</h2>
                <div class="mt-4">
                    <div class="max-w-xl text-sm text-gray-600">
                        <p>Once your account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.</p>
                    </div>

                    <div class="mt-5">
                        <button type="button" class="px-4 py-2 text-sm font-medium text-white bg-red-600 rounded-md hover:bg-red-500" wire:click="confirmUserDeletion" wire:loading.attr="disabled">Delete Account</button>
                    </div>

                    <div x-data="{ confirmingUserDeletion: @entangle('confirmingUserDeletion') }" x-on:confirming-delete-user.window="setTimeout(() => $refs.password.focus(), 250)">
                    <!-- <div x-data="{}" x-on:confirming-delete-user.window="setTimeout(() => $refs.password.focus(), 250)"> -->
                        <div x-show="confirmingUserDeletion">
                            <div class="fixed inset-0 z-10 flex items-center justify-center w-screen h-screen bg-gray-900 bg-opacity-75">
                                <div class="w-full max-w-md p-6 mx-auto bg-white rounded-md shadow-md">
                                    <h2 class="text-lg font-medium text-gray-900">Delete Account</h2>
                                    <div class="mt-4">
                                        <!-- <p>Are you sure you want to delete your account? Once your account is deleted, all of its resources and data will be permanently deleted. Please enter your password to confirm you would like to permanently delete your account.</p> -->
                                        <p>Are you sure you want to delete this account? Once this account is deleted, all of its resources and data will be permanently deleted.</p>
                                    </div>
                                    <!-- <div class="mt-4">
                                        <input type="password" class="block w-full mt-1 rounded-md border-gray-300 shadow-sm focus:border-red-300 focus:ring focus:ring-red-200 focus:ring-opacity-50" placeholder="Password" x-ref="password" wire:model.defer="deletePassword" />
                                        @error('deletePassword') <span class="text-red-500 text-sm">{{ $message }}</span> @enderror
                                    </div> -->
                                    <div class="mt-6">
                                        <button type="button" class="px-4 py-2 mr-2 text-sm font-medium text-gray-700 bg-gray-300 rounded-md hover:bg-gray-200" wire:click="$toggle('confirmingUserDeletion')" wire:loading.attr="disabled">Cancel</button>
                                        <button type="button" class="px-4 py-2 text-sm font-medium text-white bg-red-600 rounded-md hover:bg-red-500" wire:click="deleteUser" wire:loading.attr="disabled">Delete Account</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
    </div>
</div>